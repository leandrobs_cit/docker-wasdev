FROM debian:jessie

# psmisc - Needed to supply WASDEV installer dependency (killall)
RUN apt-get update && apt-get install -y psmisc sudo

# Needed to install WASDEV
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Create a regular user without password, with sudo group
RUN useradd -m wasdev -p "" && adduser wasdev sudo

RUN mkdir /opt/IBM && chown wasdev:wasdev /opt/IBM

WORKDIR /tmp
COPY package/was.7000.wasdev.nocharge.linux.amd64.tar.gz wasdev.tar.gz
COPY config/wasdev.responsefile.properties wasdev.responsefile.properties
COPY config/install.sh install.sh

USER wasdev
RUN sh install.sh

WORKDIR /opt/IBM/WebSphere/AppServer

