#!/bin/bash

set -e

echo "Unpacking wasdev file..."
mkdir wasdev && tar -vzxf wasdev.tar.gz -C wasdev

echo "Installing wasdev..."
cd wasdev/WAS && time { 
    ./install -options /tmp/wasdev.responsefile.properties -silent; 
}

echo "Removing temp files"
sudo rm -rfv /tmp/wasdev.tar.gz /tmp/wasdev /tmp/wasdev.responsefile.properties \
        /tmp/osgi_instance_location /home/wasdev/waslogs /tmp/install.sh

echo "Done!"
